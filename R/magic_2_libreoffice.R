#' Create a libreoffice document with the carts of a folder
#'
#' Create a libreoffice document with the carts of a folder
#' @param mazo Path to the images of the deck
#' @details
#' Given a path with card pictures create a libreoffice document
#' called "mazo.docx" with the card ready to print
#'
#' 
#' @examples
#'\dontrun{
#' magic_2_libreoffice('venenos')
#' ## it will create a "venenos.docx" document
#' }
#' @author person("Angel", "Martinez-Perez",
#' email = "angelmartinez@protonmail.com",
#' role = c("aut", "cre"),
#' comment = c(ORCID = "0000-0002-5934-1454"))
#'
#' @export
magic_2_libreoffice <- function(mazo) {
    checkmate::assert_directory_exists(mazo)
    out_doc <- paste0(mazo, ".docx")

    image_paths <- list.files(mazo, pattern = ".png", full.names = TRUE)

    ## Lee y reescala las imágenes
    scaled_images <- lapply(image_paths, function(path) {
        image <- magick::image_read(path)
        ## 372x520 = 6.3 cm x 8.8 cm en píxeles a 150 dpi
        image <- magick::image_scale(image, "372x520!")  
        tempfile_img <- tempfile(fileext = ".png")
        magick::image_write(image, tempfile_img)
        tempfile_img
    })

    ## Crear un nuevo documento de LibreOffice Writer con márgenes pequeños
    default_sect_properties <- officer::prop_section(
        page_margins = officer::page_mar(
            top = 1 / 2.54,
            bottom = 0.5 / 2.54, 
            left = 0, right = 0.82 / 2.54)
    )
    
    ## Número de imágenes por fila y por página
    images_per_row <- 3

    doc <- officer::read_docx() %>%
        officer::body_set_default_section(default_sect_properties)
    
    ## Insertar las imágenes en el documento en forma de columnas
    for (i in seq_along(scaled_images)) {
        img_path <- scaled_images[[i]]
        
        portrait_3_columns <- officer::block_section(
            officer::prop_section(
                page_size = officer::page_size(orient = "portrait"),
                type = "continuous",
                section_columns = officer::section_columns(
                    widths = rep(6.3 / 2.54, images_per_row),
                    space = 0)))
        
        doc <- doc %>%
            officer::body_add_img(
                src = img_path,
                width = 6.3 / 2.54,
                height = 8.8 / 2.54,
                pos = "after")    
    }
    doc <- officer::body_end_block_section(doc, value = portrait_3_columns)
    
    # Guardar el documento
    print(doc, target = out_doc)
    return(invisible())
}
    



