#!/bin/bash

/* filtro automatico para hacer una correccion de color-balance */

# Verificar si se ha proporcionado un argumento
if [ "$#" -ne 1 ]; then
    echo "Uso: $0 /ruta/a/la/carpeta"
    exit 1
fi

INPUT_FOLDER=$1

# Verificar si la carpeta existe
if [ ! -d "$INPUT_FOLDER" ];then
    echo "La carpeta $INPUT_FOLDER no existe."
    exit 1
fi

for file in "$INPUT_FOLDER"/*.png
do
    gimp -i -b - <<EOF
        (let* (
            (image (car (gimp-file-load RUN-NONINTERACTIVE "$file" "$file")))
            (drawable (car (gimp-image-get-active-layer image)))
        )
        (gimp-color-balance image 0 0 0 0 0)
        (gimp-file-save RUN-NONINTERACTIVE image drawable "$file" "$file")
        (gimp-image-delete image)
        )
        (gimp-quit 0)
EOF
done

